use errors::Result;
use gtk;
use gtk::prelude::*;
use util::get_object;

pub fn setup_login<F>(application: gtk::Application, on_accept: F) -> Result<()>
where
    F: FnOnce(String, String) + 'static,
{
    use std::cell::Cell;

    let builder = gtk::Builder::new_from_string(include_str!("login.ui"));
    let window: gtk::Window = get_object(&builder, "window")?;
    let on_accept = Cell::new(Some(on_accept));

    {
        let window = window.clone();
        let cancel_button: gtk::Button = get_object(&builder, "cancel")?;
        cancel_button.connect_clicked(move |_button| window.destroy());
    }

    {
        let window = window.clone();
        let confirm_button: gtk::Button = get_object(&builder, "confirm")?;
        let name_entry: gtk::Entry = get_object(&builder, "name")?;
        let password_entry: gtk::Entry = get_object(&builder, "password")?;

        confirm_button.connect_clicked(move |_button| {
            let name = name_entry.get_text().unwrap();
            let password = password_entry.get_text().unwrap();

            if let Some(on_accept) = on_accept.replace(None) {
                on_accept(name, password);
            }

            window.destroy();
        });
    }

    window.set_application(&application);
    window.show_all();
    Ok(())
}
