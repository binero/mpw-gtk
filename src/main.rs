#[macro_use]
extern crate error_chain;
extern crate gdk;
extern crate gio;
extern crate gtk;
extern crate mpw;

mod errors;
mod model;
mod util;
mod login;
mod mainview;

use gio::prelude::*;

const APPLICATION_ID: &'static str = "eu.jeroenbollen.mpw-gtk";

fn main() {
    let application = gtk::Application::new(APPLICATION_ID, gio::ApplicationFlags::empty())
        .expect("Failed to initialise GtkApplication");

    application.connect_activate(|app| {
        let app = app.clone();
        util::unwrap_or_halt(login::setup_login(app.clone(), |name, password| {
            util::unwrap_or_halt(mainview::setup_main_window(app, name, password));
        }));
    });

    application.run(&[]);
}
