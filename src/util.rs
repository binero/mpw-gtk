use errors::{ErrorKind, Result};
use gtk;

pub fn get_object<T>(builder: &gtk::Builder, id: &str) -> Result<T>
where
    T: gtk::IsA<gtk::Object>,
{
    builder
        .get_object(id)
        .ok_or(ErrorKind::GtkObjectMissing(id.into()).into())
}

pub fn unwrap_or_halt<T>(result: Result<T>) -> T {
    match result {
        Ok(result) => result,
        Err(error) => {
            use error_chain::ChainedError;

            eprintln!("{}", error.display_chain().to_string());
            panic!("The application ended after an error occured.");
        }
    }
}
