error_chain!{
    foreign_links {
        Gtk(::gtk::Error);
    }
    errors {
        GtkObjectMissing(object_id: String) {
            description("GtkBuilder does not have the requested object."),
            display("GtkBuilder does not have an object with id '{}' of the requested type.",
                object_id),
        }
    }
}
