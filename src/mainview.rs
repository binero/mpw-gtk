use errors::Result as AppResult;
use gdk;
use gtk;
use gtk::prelude::*;
use model::Entry;
use std::cell::Cell;
use std::rc::Rc;
pub fn setup_main_window(
    application: gtk::Application,
    name: String,
    password: String,
) -> AppResult<()> {
    use util::get_object;

    let builder = gtk::Builder::new_from_string(include_str!("main.ui"));
    let window: gtk::Window = get_object(&builder, "window")?;
    let entry_model: gtk::ListStore = get_object(&builder, "entry-store-raw")?;
    let search_bar: gtk::SearchBar = get_object(&builder, "search-bar")?;

    let is_editing = make_columns_editable(
        entry_model.clone(),
        search_bar.clone(),
        get_object(&builder, "entries.name-renderer")?,
        get_object(&builder, "entries.key-renderer")?,
    );
    connect_search_mode(
        window.clone(),
        search_bar,
        get_object(&builder, "hbutton-search")?,
        is_editing,
    );
    connect_entry_filter(
        get_object(&builder, "entry-store-filterer")?,
        get_object(&builder, "search-entry")?,
    );

    initialise_entries(entry_model);
    setup_password_copy(get_object(&builder, "entries")?, &name, &password);

    window.set_application(&application);
    window.show_all();
    Ok(())
}

fn get_entry_list() -> Vec<Entry> {
    vec![
        Entry {
            name: "Twitter".into(),
            key: "twitter.com".into(),
        },
        Entry {
            name: "Reddit".into(),
            key: "reddit.com".into(),
        },
        Entry {
            name: "Stack".into(),
            key: "binero.stackstorage.com".into(),
        },
        Entry {
            name: "TransIP".into(),
            key: "transip.be".into(),
        },
    ]
}

fn connect_search_mode<W, B>(
    window: W,
    search_bar: B,
    search_toggle: gtk::ToggleButton,
    is_editing: Rc<Cell<bool>>,
) where
    W: gtk::WidgetExt,
    B: gtk::SearchBarExt + Clone + 'static,
{
    {
        let search_bar = search_bar.clone();
        search_toggle.connect_toggled(move |search_toggle| {
            search_bar.set_search_mode(search_toggle.get_active())
        });
    }

    {
        let search_bar = search_bar.clone();
        window.connect_key_press_event(move |_, event| {
            if !is_editing.get() {
                gtk::Inhibit(search_bar.handle_event(event))
            } else {
                gtk::Inhibit(false)
            }
        });
    }

    search_bar.connect_property_search_mode_enabled_notify(move |search_bar| {
        let search_mode_enabled = search_bar.get_search_mode();
        search_toggle.set_active(search_mode_enabled);
    });
}

fn connect_entry_filter(model: gtk::TreeModelFilter, search_entry: gtk::Entry) {
    use std::cell::RefCell;
    use std::rc::Rc;

    let search_pattern = Rc::new(RefCell::new(String::new()));

    {
        let search_pattern = search_pattern.clone();
        model.set_visible_func(move |model, iter| {
            let entry_key = model
                .get_value(iter, 1)
                .get::<String>()
                .expect("Search pattern does not exist.");

            entry_key.contains(&*search_pattern.borrow())
        });
    }

    search_entry.connect_changed(move |search_entry| {
        *search_pattern.borrow_mut() = search_entry.get_text().unwrap().to_lowercase();
        model.refilter()
    });
}

fn make_columns_editable(
    model: gtk::ListStore,
    search_bar: gtk::SearchBar,
    name_renderer: gtk::CellRendererText,
    key_renderer: gtk::CellRendererText,
) -> Rc<Cell<bool>> {
    let is_editing = Rc::new(Cell::new(false));

    {
        let bind_column = |index, renderer: gtk::CellRendererText| {
            let model = model.clone();
            let search_bar = search_bar.clone();

            renderer.connect_edited(move |_, path, new_text| {
                model.set_value(
                    &model
                        .get_iter(&path)
                        .expect("Edited signal fired on non-existing row."),
                    index,
                    &new_text.into(),
                )
            });

            renderer.connect_editing_started(move |_, _, _| search_bar.set_search_mode(false));
            {
                let is_editing = is_editing.clone();
                renderer.connect_editing_started(move |_, _, _| is_editing.set(true));
            }
            {
                let is_editing = is_editing.clone();
                renderer.connect_editing_canceled(move |_| is_editing.set(false));
            }
        };
        bind_column(0, name_renderer);
        bind_column(1, key_renderer);
    }
    return is_editing;
}

fn setup_password_copy(tree_view: gtk::TreeView, name: &str, password: &str) {
    use mpw::{SeedGenerator, TEMPLATES_PIN};

    let password_generator = Box::new(SeedGenerator::new(name.as_bytes(), password.as_bytes()));

    tree_view.connect_key_press_event(move |tree_view, event| {
        let key = event.get_keyval();
        let modifiers = event.get_state();

        let copying = key == gdk::enums::key::c && modifiers.intersects(gdk::CONTROL_MASK);

        if copying {
            if let Some((entries_model, row)) = tree_view.get_selection().get_selected() {
                let key = entries_model.get_value(&row, 1);
                let seed = password_generator
                    .calculate_password_seed(key.get::<&str>().unwrap().as_bytes(), 1);

                let password = seed.create_password(TEMPLATES_PIN);

                let clipboard = gtk::Clipboard::get(&gdk::Atom::intern("CLIPBOARD"));
                clipboard.set_text(&password);
                clipboard.store();
            }
        }

        gtk::Inhibit(copying)
    });
}

fn initialise_entries(model: gtk::ListStore) {
    for (_, entry) in get_entry_list().iter().enumerate() {
        model.insert_with_values(
            None,
            &[0, 1, 2],
            &[&entry.name, &entry.key, &String::from("PIN")],
        );
    }
}
