# GTK Master Password

A GTK frontend for the [master password algorithm](http://masterpasswordapp.com/algorithm.html)

## Running

Clone the git repository, and then run it using the following command. Make sure to run it in release mode. The cryptographical backend is very slow in debug mode, which will cause excessive loading times.

```sh
cargo run --release
```

## Screenshots

![Login Screen](screenshots/login.png "Login Screen")

![Password Database](screenshots/database.png "Password Database")